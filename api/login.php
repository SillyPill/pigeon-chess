<?php
//
// required headers
header("Access-Control-Allow-Origin: http://localhost:8000/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// database connection will be here

// files needed to connect to database
include_once '../config/database.php';
/* fwrite(STDOUT, 'no errors here\n'); */
include_once '../objects/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate user object
$user = new User($db);

// get posted data
$data = json_decode(file_get_contents("php://input"));

// set product property values
$user->email = $data->email;
$email_exists = $user->emailExists();

include_once '../config/core.php';
include_once '../vendor/autoload.php';
use \Firebase\JWT\JWT;

// check if email exists and if password is correct
if($email_exists && password_verify($data->password, $user->password)){

	$token = array(
		"iss" => $iss,
		"aud" => $aud,
		"iat" => $iat,
		"nbf" => $nbf,
		"data" => array(
			"id" => $user->id,
			"uname" => $user->uname,
			"elo" => $user->elo,
			"email" => $user->email
		)
	);

	// set response code
	http_response_code(200);

	// generate jwt
	$jwt = JWT::encode($token, $key);
	echo json_encode(
		array(
			"message" => "Successful login.",
			"jwt" => $jwt
		)
	);

}

else {

	// set response code
	http_response_code(401);

	// tell the user login failed
	echo json_encode(array("message" => "Login failed."));
}

?>
